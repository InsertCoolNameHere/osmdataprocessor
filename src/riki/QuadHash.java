package riki;

import org.openstreetmap.josm.data.coor.QuadTiling;

public class QuadHash {
	public final static double LATITUDE_RANGE = 85.0511;
	public final static double LONGITUDE_RANGE = 180;
	
	public final static int GALL_PETERS_RANGE_X = 800;
	public final static int GALL_PETERS_RANGE_Y = 512;
	
	public final static char[] charMap = { '0', '1', '2', '3'};
	private static final int BITS_PER_CHAR = 2;
	
	
	public static void main(String[] args) {
		// GIVEN (x,y,zoom) GET THE GEOHASH
		System.out.println(QuadHash.getQuadHash(1, 1, 2));
		// GIVEN A COORDINATE, GET THE GEOHASH STRING
		System.out.println(encode(60, -75, 2));
		// GIVEN (x,y,zoom) GET THE BOUNDING BOX
		System.out.println(tile2boundingBox(1, 1, 2));
	}
	
	public static SpatialRange tile2boundingBox(final int x, final int y, final int zoom) {
		
		double lat2 = tile2lat(y, zoom);
		double lat1 = tile2lat(y + 1, zoom);
		double lon1 = tile2lon(x, zoom);
		double lon2 = tile2lon(x + 1, zoom);
		
		SpatialRange bb = new SpatialRange(lat1, lon1, lat2, lon2);
		return bb;
	}

	static double tile2lon(int x, int z) {
		return x / Math.pow(2.0, z) * 360.0 - 180;
	}

	static double tile2lat(int y, int z) {
		double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z);
		return Math.toDegrees(Math.atan(Math.sinh(n)));
	}
	
	/**
	 * RETURNS TILE NUMBER FOR A GIVEN COORDINATE
	 * @author sapmitra
	 * @param lat
	 * @param lon
	 * @param zoom
	 * @return
	 */
	public static BasicCoordinates getTileNumber(final double lat, final double lon, final int zoom) {
		int xtile = (int) Math.floor((lon + 180) / 360 * (1 << zoom));
		int ytile = (int) Math
				.floor((1 - Math.log(Math.tan(Math.toRadians(lat)) + 1 / Math.cos(Math.toRadians(lat))) / Math.PI) / 2
						* (1 << zoom));
		if (xtile < 0)
			xtile = 0;
		if (xtile >= (1 << zoom))
			xtile = ((1 << zoom) - 1);
		if (ytile < 0)
			ytile = 0;
		if (ytile >= (1 << zoom))
			ytile = ((1 << zoom) - 1);
		return new BasicCoordinates(xtile, ytile);
	}
		 
	/**
	 * GIVEN LAT-LON, GET CORRESPONDING QUADHASH
	 * @author sapmitra
	 * @param latitude
	 * @param longitude
	 * @param zoom
	 * @return
	 */
	public static String encode(double latitude, double longitude, int zoom) {
		
		BasicCoordinates bc = getTileNumber(latitude, longitude, zoom);
		return getQuadHash((int)bc.getLatitude(), (int)bc.getLongitude(), zoom);
	}
	
	
	public static String getQuadHash(int x, int y, int zoom) {
		
		char[] latBits = getBinaryRepresentation(y, zoom);
		char[] lonBits = getBinaryRepresentation(x, zoom);
		
		String bitRepresentation = "";
		for(int i=0; i < lonBits.length; i++) {
			bitRepresentation += latBits[i]+""+lonBits[i];
		}
		
		return encodeBits(bitRepresentation);
		
	}

	/**
	 * GIVEN A INT NUMBER, CONVERT IT TO A BINARY CHAR ARRAY
	 * @author sapmitra
	 * @param y
	 * @param precision
	 * @return
	 */
	public static char[] getBinaryRepresentation(int y, int precision) {
		
		String bitString = Integer.toBinaryString(y);
		
		for(int i = bitString.length(); i < precision; i++) {
			bitString = "0"+bitString;
		}
		return bitString.toCharArray();
	}



	/**
	 * BIT-STRING REPRESENTATION OF QUADGRID
	 * @author sapmitra
	 * @param bits
	 * @return
	 */
	public static String encodeBits(String bits) {
		
		String hash = "";

		for (int p = 0; p < bits.length(); p+=BITS_PER_CHAR) {

			int charBits = 0;
			for (int b = 0; b < BITS_PER_CHAR; ++b) {
				int bit = p + b;

				charBits <<= 1;

				if (bits.charAt(bit) == '1') {
					charBits |= 1;
				}
			}

			hash += charMap[charBits];
		}

		return hash;
	}
	
}
