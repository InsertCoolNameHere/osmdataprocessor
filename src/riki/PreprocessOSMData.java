package riki;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PreprocessOSMData {

	public static void main(String[] args) throws IOException {
		
		PreprocessOSMData p = new PreprocessOSMData();
		String ipDir = "/s/chopin/b/grad/sapmitra/Documents/fove/";
		String opDir = "/s/chopin/b/grad/sapmitra/Documents/grad544/";
		if(args.length > 0) {
			ipDir = args[0];
			opDir = args[1];
		}
		
		p.alterCsv(ipDir, opDir);
	}
	
	public void alterCsv(String ipDirName, String opDirName) throws IOException {
		
		File file = new File(ipDirName);
		
		FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
            	
            	return pathname.getAbsolutePath().endsWith(".txt");
            	
            }
         };
		
		if (file.isDirectory()) {
			File[] files = file.listFiles(filter);
			
			FileInputStream inputStream = null;
			Scanner sc = null;
			
			int cnt = 0;
			
			for(File f : files) {
				
				StringBuffer allLines = new StringBuffer("");
				
				String filepath = f.getAbsolutePath();
				
				String filename = f.getName();
				
				String outputPath = opDirName+"/"+filename;
				
				//if(cnt % 5 == 0)
					System.out.println(cnt+":READING: "+f.getName());
				cnt++;
				
				inputStream = new FileInputStream(filepath);
				sc = new Scanner(inputStream);
				
				while (sc.hasNextLine()) {
					
					String line = sc.nextLine();
					
					String[] tokens = line.split("\\s+");
					
					String coordinates = tokens[0];
					String count = tokens[1];
					
					String[] coordinateTokens = coordinates.split("/");
					
					int zoom = Integer.valueOf(coordinateTokens[0]);
					
					if(zoom==0 || zoom > 16)
						continue;
					String x = coordinateTokens[1];
					String y = coordinateTokens[2];
					
					String quadhash = QuadHash.getQuadHash(Integer.valueOf(x), Integer.valueOf(y),zoom);
					
					String updatedLine = zoom+","+x+","+y+","+quadhash+","+count+"\n";
					
					allLines.append(updatedLine);
					
					
				}
				inputStream.close();
				
				FileWriter fileWriter = new FileWriter(outputPath, false);
				PrintWriter printWriter = new PrintWriter(fileWriter);
				printWriter.print(allLines.toString());
				printWriter.close();
				
				
			}
			
			
		}
		
	}

}
