# README #

This project deals with pre-processing of the Open Street-Map data detailed [here](http://lukasmartinelli.ch/python/2015/05/24/parsing-and-visualizing-osm-access-logs.html). The records are processed to convert the quad tile coordinate to a geohash-like string called QuadHash. It also contains utility functions for QuadTiles and QuadHash in the Quadhash.java class.

QuadHash.java is based on the [wiki](https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames).